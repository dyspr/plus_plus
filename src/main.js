var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.08
var dimension = 9
var frame = 0
var clockWise = true

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  if (clockWise === true) {
    fill(colors.dark)
  } else {
    fill(255)
  }
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  push()
  if (clockWise === true) {
    for (var i = 0; i < dimension; i++) {
      for (var j = 0; j < dimension; j++) {
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize * sqrt(pow(1, 2) + pow((1 / 3), 2)), windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize  * sqrt(pow(1, 2) + pow((1 / 3), 2)))
        rotate(Math.PI * frame * 0.5)
        rotate(0.32175055)
        drawPlus(boardSize * initSize, 255)
        pop()
      }
    }
  } else {
    for (var i = 0; i < dimension + 1; i++) {
      for (var j = 0; j < dimension + 1; j++) {
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5) - 0.5) * boardSize * initSize * sqrt(pow(1, 2) + pow((1 / 3), 2)), windowHeight * 0.5 + (j - Math.floor(dimension * 0.5) - 0.5) * boardSize * initSize * sqrt(pow(1, 2) + pow((1 / 3), 2)))
        rotate(-Math.PI * frame * 0.5)
        rotate(0.32175055)
        drawPlus(boardSize * initSize, 0)
        pop()
      }
    }
  }
  pop()

  // masking
  fill(colors.light)
  noStroke()
  rect(windowWidth * 0.5, (windowHeight - boardSize) * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect(windowWidth * 0.5, windowHeight * 0.75 + boardSize * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect((windowWidth - boardSize) * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)
  rect(windowWidth * 0.75 + boardSize * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)

  frame += deltaTime * 0.00025
  if (frame >= 1) {
    frame = 0
    if (clockWise === true) {
      clockWise = false
    } else {
      clockWise = true
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function drawPlus(size, col) {
  fill(col)
  noStroke()
  beginShape()
  vertex((-1) * size * (1 / 6), (-1) * size * (3 / 6))
  vertex((+1) * size * (1 / 6), (-1) * size * (3 / 6))
  vertex((+1) * size * (1 / 6), (-1) * size * (1 / 6))
  vertex((+1) * size * (3 / 6), (-1) * size * (1 / 6))
  vertex((+1) * size * (3 / 6), (+1) * size * (1 / 6))
  vertex((+1) * size * (1 / 6), (+1) * size * (1 / 6))
  vertex((+1) * size * (1 / 6), (+1) * size * (3 / 6))
  vertex((-1) * size * (1 / 6), (+1) * size * (3 / 6))
  vertex((-1) * size * (1 / 6), (+1) * size * (1 / 6))
  vertex((-1) * size * (3 / 6), (+1) * size * (1 / 6))
  vertex((-1) * size * (3 / 6), (-1) * size * (1 / 6))
  vertex((-1) * size * (1 / 6), (-1) * size * (1 / 6))
  endShape()
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
